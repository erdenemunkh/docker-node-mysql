const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const router = express.Router();
require("dotenv").config();
const { Sequelize, DataTypes } = require("sequelize");
const cors = require("cors");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.options("*", cors());

const DATABASE_NAME = process.env.DATABASE_NAME;
const DATABASE_USER = process.env.DATABASE_USER;
const DATABASE_PASSWORD = process.env.DATABASE_PASSWORD;

const sequelize = new Sequelize(
  DATABASE_NAME,
  DATABASE_USER,
  DATABASE_PASSWORD,
  {
    host: process.env.DATABASE_HOST,
    dialect: "mysql",
  }
);

sequelize.authenticate().then(() => {
  console.log("Connection esstablished");
});

const PORT = process.env.APP_PORT;
const HOST = "0.0.0.0";

const User = sequelize.define(
  "User",
  {
    username: {
      type: DataTypes.STRING,
    },
    userpwd: {
      type: DataTypes.STRING,
    },
  },
  {
    tableName: "users",
  }
);

app.get("/", function (_, res) {
  return res.status(200).send({ message: "Hello World!" });
});

app.get("/user", async (_, res) => {
  return await User.findAll()
    .then((user) => res.status(200).send(user))
    .catch((err) => console.log(err));
});

app.post("/user", async (req, res) => {
  const userName = req.body.username;
  const userPwd = req.body.userpwd;
  const createdUser = await createUser(userName, userPwd);
  return res.status(200).send(createdUser);
});

app.delete("/user/:id", async (req, res) => {
  await deleteUser(Number(req.params.id));
  return res.status(200).send("Success");
});

async function createUser(userName, userPwd) {
  return await User.build({ username: userName, userpwd: userPwd }).save();
}

async function deleteUser(userId) {
  return await User.destroy({
    where: {
      id: userId,
    },
  });
}

app.listen(PORT, HOST, () => {
  console.log(`Server is runnig at ${HOST}:${PORT}`);
});
